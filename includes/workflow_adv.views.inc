<?php


/**
 * @file
 * Provide views handler information for workflow_adv.module.
 */

/**
 * 
 * Includes the 'workflow_adv_node_history' table.
 * 
 */

/**
 * Implementation of hook_views_data().
 */

function workflow_adv_views_data() {
  
  $data['workflow_adv_node_history']['table']['group']  = t('Workflow Node access');

  // For other base tables, explain how we join
  $data['workflow_adv_node_history']['table']['join'] = array(

    // Directly links to node table.
    'node' => array(
      'field' => 'nid',
      //'left_table' => 'node_access',
      'left_field' => 'nid',
    ),
  );
  // nid field
  $data['workflow_adv_node_history']['gid'] = array(
    
    // Directly links to node table.
    'title' => t('Workflow Node Access'),
    'help' => t('Filter by Workflow group access.'),
    'filter' => array(
      'handler' => 'workflow_adv_views_handler_filter_node_access',
      'numeric' => TRUE,
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function workflow_adv_views_handlers() {

  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'workflow_adv') . '/includes',
    ),
    'handlers' => array(      
      // filter handlers
      'workflow_adv_views_handler_filter_node_access' => array(
        'parent' => 'views_handler_filter',
      ),
      // relationship handlers
      // sort handlers
    ),
  );
}