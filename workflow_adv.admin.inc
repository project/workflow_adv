<?php


/**
 * Menu callback: administer roles.
 *
 * @ingroup forms
 * @see user_admin_role_validate()
 * @see user_admin_role_submit()
 * @see theme_user_admin_new_role()
 */
function workflow_adv_groups_role() {
  $rid = arg(5);
  if ($rid) {
    if ($rid == DRUPAL_ANONYMOUS_RID || $rid == DRUPAL_AUTHENTICATED_RID) {
      drupal_goto('admin/user/roles');
    }
    // Display the edit role form.
    $role = db_fetch_object(db_query('SELECT * FROM {role} WHERE rid = %d', $rid));
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Role name'),
      '#default_value' => $role->name,
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#description' => t('The name of the workflow group. Example: "Moderator Group", "Editorial Board Group", "Site Architect Group".<br> As a best practice use "Group" at the end of each group you add.'),
    );
    $form['rid'] = array(
      '#type' => 'value',
      '#value' => $rid,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save role'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete role'),
    );
  }
  else {
    $form['name'] = array(
      '#type' => 'textfield',
      '#size' => 32,
      '#maxlength' => 64,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add role'),
    );
    $form['#submit'][] = 'workflow_adv_groups_role_submit';
    $form['#validate'][] = 'workflow_adv_groups_role_validate';
  }
  return $form;
  //return theme('workflow_adv_groups_new_role',$form);
}

function workflow_adv_groups_new_role() {
  $rid = arg(5);
  if ($rid) {
    if ($rid == DRUPAL_ANONYMOUS_RID || $rid == DRUPAL_AUTHENTICATED_RID) {
      drupal_goto('admin/user/roles');
    }
    // Display the edit role form.
    $role = db_fetch_object(db_query('SELECT * FROM {role} WHERE rid = %d', $rid));
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Role name'),
      '#default_value' => $role->name,
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#description' => t('The name of the workflow group. Example: "Moderator Group", "Editorial Board Group", "Site Architect Group".<br> As a best practice use "Group" at the end of each group you add.'),
    );
    $form['rid'] = array(
      '#type' => 'value',
      '#value' => $rid,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save role'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete role'),
    );
  }
  else {
    $form['name'] = array(
      '#type' => 'textfield',
      '#size' => 32,
      '#maxlength' => 64,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add role'),
    );
    $form['#submit'][] = 'workflow_adv_groups_role_submit';
    $form['#validate'][] = 'workflow_adv_groups_role_validate';
  }
  return $form;
  //return theme('workflow_adv_groups_new_role',$form);
}


function workflow_adv_groups_role_validate($form, &$form_state) {
  if ($form_state['values']['name']) {
    if ($form_state['values']['op'] == t('Save role')) {
      if (db_result(db_query("SELECT COUNT(*) FROM {role} WHERE name = '%s' AND rid != %d", $form_state['values']['name'], $form_state['values']['rid']))) {
        form_set_error('name', t('The role name %name already exists. Please choose another role name.', array('%name' => $form_state['values']['name'])));
      }
      
      if (substr($form_state['values']['name'], -5) !== "Group") {
        form_set_error('name', t('You must specify a valid role name. Ending with keyword "Group", e.g. Article Author-Editor Group'));
        
      }
    }
    else if ($form_state['values']['op'] == t('Add role')) {
      if (db_result(db_query("SELECT COUNT(*) FROM {role} WHERE name = '%s'", $form_state['values']['name']))) {
        form_set_error('name', t('The role name %name already exists. Please choose another role name.', array('%name' => $form_state['values']['name'])));
      }
      //$pos = strpos($form_state['values']['name'], "Group");
      if (substr($form_state['values']['name'], -5) !== "Group") {
        form_set_error('name', t('You must specify a valid role name. Ending with keyword "Group", e.g. Article Author-Editor Group'));
        
      }
    }
  }
  else {
    form_set_error('name', t('You must specify a valid role name. Ending with keyword "Group", e.g. Article Author-Editor Group'));
  }
}

function workflow_adv_groups_role_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save role')) {
    db_query("UPDATE {role} SET name = '%s' WHERE rid = %d", $form_state['values']['name'], $form_state['values']['rid']);
    drupal_set_message(t('The role has been renamed.'));
  }
  else if ($form_state['values']['op'] == t('Delete role')) {
    db_query('DELETE FROM {role} WHERE rid = %d', $form_state['values']['rid']);
    db_query('DELETE FROM {permission} WHERE rid = %d', $form_state['values']['rid']);
    // Update the users who have this role set:
    db_query('DELETE FROM {users_roles} WHERE rid = %d', $form_state['values']['rid']);

    drupal_set_message(t('The role has been deleted.'));
  }
  else if ($form_state['values']['op'] == t('Add role')) {
    db_query("INSERT INTO {role} (name) VALUES ('%s')", $form_state['values']['name']);
    drupal_set_message(t('The role has been added.'));
    $db_rid = db_result(db_query("SELECT rid FROM {role} WHERE name = '%s'", $form_state['values']['name']));
    db_query("INSERT INTO {permission} VALUES (0,%d,'%s',0)", $db_rid, "workflow roles");
    drupal_set_message(t('The permission that differentiate this role with other roles has been added.'));
  }
  $form_state['redirect'] = 'admin/build/workflow/groups';
  return;
}

function theme_workflow_adv_groups_new_role($form) {
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => 2));
  $workflow_roles = user_roles(0,"workflow roles");
  foreach ($workflow_roles as $rid => $name) {
    $edit_permissions = l(t('edit permissions'), 'admin/user/permissions/'. $rid);
    if (!in_array($rid, array(DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID))) {
      //$rows[] = array($name, l(t('edit role'), 'admin/build/workflow/groups/edit/'. $rid), $edit_permissions);
      $rows[] = array($name, l(t('edit role'), 'admin/build/workflow/groups/edit/'. $rid));
    }
    else {
      //$rows[] = array($name, t('locked'), $edit_permissions);
    }
  }
  $rows[] = array(drupal_render($form['name']), array('data' => drupal_render($form['submit']), 'colspan' => 2));

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows);

  return $output;
}

